import cv2
import numpy as np


def get_location(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONUP:
        global imgHSV
        print("Clicked", x, y, imgHSV[y][x])


def setMinColorRed(x):
    global minColor
    minColor[0] = x


def setMinColorGreen(x):
    global minColor
    minColor[1] = x


def setMinColorBlue(x):
    global minColor
    minColor[2] = x


def setMaxColorRed(x):
    global maxColor
    maxColor[0] = x


def setMaxColorGreen(x):
    global maxColor
    maxColor[1] = x


def setMaxColorBlue(x):
    global maxColor
    maxColor[2] = x


def findObject():
    global imgHSV, imgGray, minColor, maxColor

    # Mask the object
    mask = cv2.inRange(imgHSV, minColor, maxColor)

    # Convert to a grayscale image
    imgGray = cv2.cvtColor(cv2.bitwise_and(imgHSV, imgHSV, mask=mask), cv2.COLOR_BGR2GRAY)
    imgGray = cv2.adaptiveThreshold(imgGray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)

    # Dilate and Erode using large ellipses
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (16, 16))
    closing = cv2.morphologyEx(imgGray, cv2.MORPH_CLOSE, kernel)
    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel)

    # Dilate using small rectangles
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (8, 8))
    imgGray = cv2.dilate(opening, kernel, iterations=1)


def show_webcam(mirror=False):
    # Capture video
    camera = cv2.VideoCapture(0)
    while True:
        # Convert into stream
        global img
        return_val, img = camera.read()
        if mirror:
            img = cv2.flip(img, 1)

        # Display the webcam
        cv2.imshow('Webcam', img)
        # cv2.resizeWindow('Webcam', 512, 512)

        # Convert and display HSV video
        global imgHSV
        imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        findObject()
        cv2.imshow('HSV Video', imgHSV)

        # Convert and display Gray video
        global imgGray
        cv2.imshow('Gray Video', imgGray)

        # Exit
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()
    print("Exited: " + str(return_val))


def main():
    print("OpenCV Version: " + cv2.__version__)

    global img
    cv2.namedWindow('Webcam', cv2.WINDOW_NORMAL)

    global imgGray
    cv2.namedWindow('Gray Video', cv2.WINDOW_NORMAL)

    global imgHSV
    cv2.namedWindow('HSV Video', cv2.WINDOW_NORMAL)
    cv2.setMouseCallback('HSV Video', get_location)

    global minColor
    minColor = np.array([0, 0, 0])

    global maxColor
    maxColor = np.array([255, 255, 255])

    cv2.createTrackbar('R-min', 'HSV Video', 0, 255, setMinColorRed)
    cv2.createTrackbar('R-max', 'HSV Video', 0, 255, setMaxColorRed)
    cv2.createTrackbar('G-min', 'HSV Video', 0, 255, setMinColorGreen)
    cv2.createTrackbar('G-max', 'HSV Video', 0, 255, setMaxColorGreen)
    cv2.createTrackbar('B-min', 'HSV Video', 0, 255, setMinColorBlue)
    cv2.createTrackbar('B-max', 'HSV Video', 0, 255, setMaxColorBlue)

    show_webcam(mirror=True)


if __name__ == '__main__':
    main()
