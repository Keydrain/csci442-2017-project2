import cv2


def show_webcam(mirror=False):
    # Capture video
    camera = cv2.VideoCapture(0)
    previousFrames = None
    while True:
        # Convert into stream
        global img
        return_val, img = camera.read()
        if mirror:
            img = cv2.flip(img, 1)

        # Blur and brighten image for easier compare
        imgBright = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
        imgBright[:, :, 0] = cv2.equalizeHist(imgBright[:, :, 0])
        imgBright = cv2.cvtColor(imgBright, cv2.COLOR_YUV2BGR)
        imgBlur = cv2.GaussianBlur(imgBright, (9, 9), 0)

        # Convert and display Gray video
        global imgGray
        imgGray = cv2.cvtColor(imgBlur, cv2.COLOR_BGR2GRAY)

        # Keep a running total for each frame (more tunable than accumulateWeighted)
        if previousFrames is None:
            previousFrames = [imgGray]
            continue
        elif len(previousFrames) < 10:
            previousFrames.append(imgGray)
        else:
            previousFrames.append(imgGray)
            previousFrames.pop(0)
        averageImg = previousFrames[0]
        for x in previousFrames:
            averageImg = cv2.addWeighted(averageImg, 0.2, x, 0.8, 0)

        # Calculate the difference between present and history
        delta = cv2.absdiff(averageImg, imgGray)
        cv2.imshow('Difference', delta)

        # Generate blobs based on difference to running average, dilate and erode for better visibility
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (9, 9))
        thresh = cv2.threshold(delta, 5, 255, cv2.THRESH_BINARY)[1]
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (21, 21))
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
        thresh = cv2.dilate(thresh, None, iterations=4)
        im2, contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for c in contours:
            # Only get big blobs
            if cv2.contourArea(c) < pow(2, 12):
                continue

            # Find rectangles and draw them
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Display the thresholds
        cv2.imshow('Motion', thresh)

        # Display the webcam
        cv2.imshow('Webcam', img)

        # Exit
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()
    print("Exited: " + str(return_val))


def main():
    print("OpenCV Version: " + cv2.__version__)

    cv2.namedWindow('Webcam', cv2.WINDOW_NORMAL)

    cv2.namedWindow('Motion', cv2.WINDOW_NORMAL)

    show_webcam(mirror=True)


if __name__ == '__main__':
    main()
